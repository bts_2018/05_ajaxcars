$(document).ready(function(){
    
    //server name
    var server ="http://www.winelove.club/cars";

    //addEventListener
    $("#formCreate").submit(processForm);
    $("#formEdit").submit(processEditForm);
    $("#tablelist").on("click",".delete-item",deleteItem);
    $("#tablelist").on("click",".edit-item",editItem);
 
   
    /************************LOAD PAGE *****************/
    $.ajax({
        dataType:'json',
        url: server + '/read.php'
    }).done(function(cars){
        console.log(cars);
        manageRows(cars);
    }).fail(function(){
        alert("Error");
    });

    function manageRows(cars){
        
        $("tbody").html("");

        $.each(cars,function(key,value) {
            var row = '';

            row = row + '<tr>';
                row = row + '<td>' + value.id + '</td>';
                row = row + '<td>' + value.name + '</td>';
                row = row + '<td data-id="' + value.id +'">';
                    row = row + '<button data-toggle="modal" data-target="#myModalEditCar" class="btn btn-danger edit-item">Edit</button> ';
                    row = row + '<button class="btn btn-primary delete-item">Delete</button> ';
                row = row + '</td>';
            row = row + '</tr>';

            $("tbody").append(row);

        });
    }
    
    function processForm(event){
        event.preventDefault();
    
        var newCar = $('#newCar').val();
        
        //ajax call
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: server + '/create.php',
            data:{name:newCar}
        }).done(function(cars){
             $('#newCar').val('');
             $(".modal").modal("hide");
             manageRows(cars);
        }).fail(function(){
            alert("error on new car");
        });

    }

    function processEditForm(event){
        event.preventDefault();
       
        var idCar = $('#idCar').val();
        var editCar = $('#editCar').val();

        // ajax call
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: server + '/update.php',
            data:{id:idCar,name:editCar}
        }).done(function(cars){
             $('#editCar').val('');
             $(".modal").modal("hide");
             manageRows(cars);
        }).fail(function(){
            alert("error on new edit");
        });
    }
    

    function deleteItem(){
        
        var idCar = $(this).parent("td").data("id");
        
        if (confirm("are you sure to delete?")) {
            //ajax call
            $.ajax({
                dataType: 'json',
                type:'GET',
                url: server + '/delete.php',
                data:{id:idCar}
            }).done(function(cars){
                manageRows(cars);
            }).fail(function(){
                alert("error on delete ");
            });

        }
       
    }


    function editItem(){
        var idCar = $(this).parent("td").data("id");
        var nameCar = $(this).closest("tr").children("td:eq(1)").text();
        
        $("#idCar").val(idCar);
        $("#editCar").val(nameCar);
        
    }
 
});